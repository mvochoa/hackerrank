****************************************
* KAREL OMI Version 1.1 (beta)       
* 04/03/2004                                     
* por Cesar Cepeda	                          
****************************************

Esta es la version oficial de Karel para la Olimpiada Mexicana de Informatica, esta es la version que se utilizara para calificar los programas que envies en los examenes de la Olimpiada.



*******************************************
CONTROL DE VERSIONES
*******************************************

////////////////////////////////
Version 1.2 
04 - 03 - 2004
////////////////////////////////

 * Las funciones booleanas se pueden utilizar con y sin parentesis, anteriormente solo funcionaban sin parentesis.

 * Al guardar un mundo, tambien se guarda el numero de zumbadores iniciales en la mochila de Karel

 * Tanto Karel como el mundo soportan la configuracion de Infinito numero de zumbadores

 * Se agregaron Hot Keys a los botones.

 * Se resolvieron algunos problemas menores con los botones de Abrir, Guardar y Guardar como


////////////////////////////////
Version 1.1 (beta)
03 - 03 - 2004
////////////////////////////////

 * Esta version ya soporta la compilacion de java.  Unicamente no soporta, al igual que el pascal las declaraciones de enlace.

 * Esta version permite navegar todo el mapa del mundo de Karel.  El mundo de Karel siempre es de 100 x 100

 * Esta version permite la ejecucion paso por paso de un codigo, asi como detener una ejecucion en cualquier momento para 
   ejecutar paso por paso



////////////////////////////////
Version 1.0 (beta)
02 - 03 - 2004
////////////////////////////////
La version actual es aun una version beta.  Por el momento unicamente soporta programacion en Pascal.  Soporta todas las funcionalidades de Karel el Robot version 1.3 excepto las "Declaraciones de Enlace".

En la version 1.0 final se soportara ademas programacion en Java.


////////////////////////////////
INSTALACION
///////////////////////////////

Para instalar el programa basta descomprimirlo en cualquier carpeta de tu disco permitiendo que el WinZip use las rutas predefinidas de los archivos.

El ejecutable del programa se llama Karel.EXE




///////////////////////////////
COMENTARIOS
//////////////////////////////

Para cualquier comentario, o si deseas cooperar en el desarrollo de este programa escribe un correo a  cesar@auronix.com
